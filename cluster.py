#!/usr/bin/env python2

import re,ping,socket,os,time,sys


### CONSTANTS
REGEX_MAC = re.compile(r"([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})")
REGEX_IP = re.compile(r"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b")


### DATA STRUCTURES
class Machine :
    def __init__(self, mac, ip, nickname) :
        self.name = nickname
        self.mac = mac
        self.ip = ip
    def __str__(self) :
        return "Machine '" + self.name + "' at " + self.ip + " has MAC address " + self.mac
    def __repr__(self) :
        return "Machine '" + self.name + "'"
    def is_up(self) :
        if ping.do_one(self.ip,4,32) :
            return True
        return False
    def start() :
        os.system("wakeonlan -i " + BROADCAST_ADDR + " " + m + " > /dev/null")
    def stop() :
        os.system("ssh -t node@" +self.ip+ " 'sudo shutdown -P 0' > /dev/null")
    def execute(command) :
        raise NotImplementedError()


### IMPORT DATA FROM DHCPD.CONF
dhcpd_conf = open("/etc/dhcp/dhcpd.conf", 'r')
dhcpd_conf_data = dhcpd_conf.read()
dhcpd_conf.close()
# Get the broadcast address
BROADCAST_ADDR = REGEX_IP.search(dhcpd_conf_data, dhcpd_conf_data.find("broadcast-address")).group()
# Get each of the machine names
host_results = re.finditer(r"host\s+", dhcpd_conf_data)
MACHINES = []
for host in host_results :
    hostname = re.compile("\w+").search(dhcpd_conf_data, host.end()).group()
    mac_addr = REGEX_MAC.search(dhcpd_conf_data, host.end()).group()
    ip_addr = REGEX_IP.search(dhcpd_conf_data, host.end()).group()
    MACHINES.append(Machine(mac_addr, ip_addr, hostname))


def start(identity) :
    macaddr = None
    if REGEX_MAC.match(identity) :
        macaddr = machine
    elif REGEX_IP.match(identity) :
        for machine in MACHINES :
            if machine.ip == identity :
                macaddr = machine.mac
                break
    else :
        for machine in Machines :
            if machine.name == identity :
                macaddr = machine.mac
                break
    if not macaddr :
        return False
    os.system("wakeonlan -i " + BROADCAST_ADDR + " " + m + " > /dev/null")




if __name__ == "__main__" :
    import Queue
    qu = Queue.Queue()
    for i in MACHINES :
        ###qu.put((i.ip,0,i.name))
        qu.put((i,0))
    on = 0
    off = 0
    while not qu.empty():
        tp = qu.get()
        if ping.do_one(tp[0].ip,0.0625,64) == None:
            if tp[1] < 5:
                tmp = tp[0],tp[1]+1
                qu.put(tmp)
            else:
                print "  [OFFLINE]\t",tp[0]
                off += 1
        else:
            print "  [ONLINE]\t",tp[0]
            on += 1
    print "cluster status: ",on," ON, ",off," OFF"



"""
class Machine :
    def isup(self) :
        for passes in xrange(0,4) :
            if ping.do_one(self.ip,1,16) :
                return True
        return False
    def start(self) :
        if not self.isup() :
            os.system("wakeonlan -i " + Machine.BROADCAST + " " + self.mac + " > /dev/null")
            time.sleep(48)
    def stop(self) :
        if self.isup() :
            os.system("ssh -t node@" +self.ip+ " 'sudo shutdown -P 0' > /dev/null")


class Machines :
    @staticmethod
    def get_all() :
        machines = []
        for addr in ["192.168.1."+`i` for i in xrange(128,255)] :
            m = Machines.get(addr)
            if m :
                machines.append(m)
        return machines
    @staticmethod
    def get(addr) :
        if not type(addr) == str :
            raise TypeError
        mach = Machine(None,None)
        leases = open("/var/lib/dhcp/dhcpd.leases","r").read()
        if re.match(r'([0-9A-Fa-f]{2}:){5}[0-9A-Fa-f]{2}', addr) or \
                re.match(r'([0-9A-Fa-f]{2}-){5}[0-9A-Fa-f]{2}', addr) or \
                re.match(r'[0-9A-Fa-f]{12}', addr) :
            # is a mac
            mach.mac = addr[:]
            pass
        elif re.match(r'([0-9]+\.){3}[0-9]+', addr) :
            # is an ip
            mach.ip = addr[:]
            try :
                minin = leases.index('lease ' + addr)
                maxin = leases.index('}', minin)
                minin = leases.index('hardware ethernet ', minin)
                if minin > maxin :
                    raise ValueError
                minin += 18
                mach.mac = leases[minin:leases.index(';',minin)]
            except ValueError :
                pass
        return mach

asdf = Machines.get_all()
for i in asdf :
    print "  " + `i.ip` + "  " + `i.mac`
"""


"""
class Machine :
    BROADCAST = "192.168.1.255"
    machines = []
    for i in xrange(128,255) :
        mach = Machine("192.168.1." + `i`)
    def __init__(self, ip) :
        self.ip = ip
        leases = open("/var/lib/dhcp/dhcpd.leases","r").read()
        try :
            minin = leases.index('lease ' + self.ip)
            maxin = leases.index('}', minin)
            minin = leases.index('hardware ethernet ', minin)
            if minin > maxin :
                raise ValueError
            minin += 18
            self.mac = leases[minin:leases.index(';',minin)]
        except ValueError :
            config = open("/etc/dhcp/dhcpd.conf")
            # FINISH THIS WITH PROPER CONFIG
            pass
        Machine.machines.append(self)
    def isup(self) :
        for passes in xrange(0,4) :
            if ping.do_one(self.ip,1,16) :
                return True
        return False
    def start(self) :
        if not self.isup() :
            os.system("wakeonlan -i " + Machine.BROADCAST + " " + self.mac + " > /dev/null")
    def stop(self) :
        if self.isup() :
            os.system("ssh node@" +self.ip+ " 'sudo shutdown -P 0' > /dev/null")

def start_all(wait = False) :
    for m in Machine.machines :
        if not m.isup() :
            os.system("wakeonlan -i " + BROADCAST + " " + m + " > /dev/null")
            time.sleep(48)
    if wait :
        while True :
            allup = True
            for m in Machine.machines :
                if not m.isup() :
                    allup = False
                    break
            if allup :
                break
            time.sleep(8)

def stop_all(wait = False) :
    command = "dsh -m "
    for m in Machine.machines :
        command += "node@" + m.ip + ","
    command = command[:-1]
    command += " 'sudo shutdown -P 0' > /dev/null"
    os.system(command)




BROADCAST = "192.168.1.255"


import time,os,sys,ping,socket

def start(maddr = MACS) :
    if type(maddr) == str :
        maddr = [maddr]
    if not type(maddr) == list :
        raise TypeError
    for m in maddr :
        os.system("wakeonlan -i " + BROADCAST + " " + m + " > /dev/null")
        time.sleep(48)

start()
"""
"""
def stop(ipaddr = ["192.168.1."+`128+i` for i in xrange(len(MACS))]) :
    if type(ipaddr) == str :
        ipaddr = [ipaddr]
    if not type(ipaddr) == list :
        raise TypeError
    for ip in ipaddr :
        os.system("ssh node@" +ip+ " 'sudo shutdown -P 0' > /dev/null")


def status(ipaddr = ["192.168.1."+`128+i` for i in xrange(len(MACS))]) :
    if type(ipaddr) == str :
        ipaddr = [ipaddr]
    if not type(ipaddr) == list :
        raise TypeError
    ipaddr = dict(zip(ipaddr, [False for i in xrange(len(ipaddr))]))
    for passes in xrange(0,4) :
        for ip in ipaddr :
            if not ipaddr[ip] :
                if ping.do_one(ip,1,16) :
                    ipaddr[ip] = True
    if len(ipaddr) == 1 :
        return ipaddr[list(ipaddr)[0]]
    return ipaddr
"""
